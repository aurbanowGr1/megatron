package domain;

import java.sql.Date;



public class StatusHistory extends Entity
{
	
	private Date receiveDate;
	
	private Date inProgress;
	
	private Date releaseDate;
	
	private Order order;
	

	public Date getReceiveDate() 
	{
		return receiveDate;
	}

	public void setReceiveDate(Date receiveDate) 
	{
		this.receiveDate = receiveDate;
	}

	public Date getInProgress() 
	{
		return inProgress;
	}

	public void setInProgress(Date inProgress) 
	{
		this.inProgress = inProgress;
	}

	public Date getReleaseDate() 
	{
		return releaseDate;
	}

	public void setReleaseDate(Date releaseDate) 
	{
		this.releaseDate = releaseDate;
	}

	public Order getOrder() 
	{
		return order;
	}

	public void setOrder(Order order) 
	{
		this.order = order;
	}
	
	
	
	
	
}
