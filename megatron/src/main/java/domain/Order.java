package domain;

import java.util.List;


public class Order extends Entity
{
	public enum MethodOfPayment
{
	cash, creditCard
}
	
	private MethodOfPayment methodOfPayment;
	
	private float costOfRepair;
	
	private String descriptionOfIssue;
	
	private Client client;
	
	private List<Employee>employee;
	
	private List<StatusHistory>statusHistory;
	
	

	public MethodOfPayment getMethodOfPayment() 
	{
		return methodOfPayment;
	}

	public void setMethodOfPayment(MethodOfPayment methodOfPayment) 
	{
		this.methodOfPayment = methodOfPayment;
	}

	public float getCostOfRepair() 
	{
		return costOfRepair;
	}

	public void setCostOfRepair(float costOfRepair) 
	{
		this.costOfRepair = costOfRepair;
	}

	public String getDescriptionOfIssue() 
	{
		return descriptionOfIssue;
	}

	public void setDescriptionOfIssue(String descriptionOfIssue) 
	{
		this.descriptionOfIssue = descriptionOfIssue;
	}

	public Client getClient() 
	{
		return client;
	}

	public void setClient(Client client) 
	{
		this.client = client;
	}

	public List<Employee> getEmployee() 
	{
		return employee;
	}

	public void setEmployee(List<Employee> employee) 
	{
		this.employee = employee;
	}

	public List<StatusHistory> getStatusHistory() 
	{
		return statusHistory;
	}

	public void setStatusHistory(List<StatusHistory> statusHistory)
	{
		this.statusHistory = statusHistory;
	}
	
	

}
