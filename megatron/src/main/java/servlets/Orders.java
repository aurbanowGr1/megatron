package servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import repositories.IRepository;
import repositories.implementations.OrderRepository;
import unitOfWork.IUnitOfWork;
import unitOfWork.UnitOfWork;
import domain.Order;
import entityBuilders.IEntityBuilder;
import entityBuilders.OrderEntityBuilder;

@WebServlet("/Orders")
public class Orders extends HttpServlet 
{
	private static final long serialVersionUID = 1L;
	private Connection connection;
       
    public Orders() 
    {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		try 
		{
			
		Class.forName("com.mysql.jdbc.Driver").newInstance();
		connection = DriverManager.getConnection("jdbc:mysql://localhost/megatron", "root","");
		
		IEntityBuilder<Order> builder = new OrderEntityBuilder();
		IUnitOfWork uow = new UnitOfWork(connection);
		IRepository<Order> repo = new OrderRepository(connection,builder,uow);
		
		List<Order> orders = repo.getAll();
		String html = "<html><body>";
		
		for(Order o : orders)
		{	
			html+= o.getId() + " " + o.getMethodOfPayment() + " " + o.getCostOfRepair() + " " + o.getDescriptionOfIssue() + " " + "<br>";
		}
		
		html+="</body></html>";
		
		
		PrintWriter out = response.getWriter();
		out.println(html);
		out.close();
		}
		catch (Exception e) 
		{
			
			e.printStackTrace();
		}
	}
}
