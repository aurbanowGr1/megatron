package servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import repositories.IRepository;
import repositories.implementations.StatusHistoryRepository;
import unitOfWork.IUnitOfWork;
import unitOfWork.UnitOfWork;
import domain.StatusHistory;
import entityBuilders.IEntityBuilder;
import entityBuilders.StatusHistoryEntityBuilder;

@WebServlet("/StatusHistories")
public class StatusHistories extends HttpServlet 
{
	private static final long serialVersionUID = 1L;
	private Connection connection;
       
    public StatusHistories() 
    {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		try 
		{
			
		Class.forName("com.mysql.jdbc.Driver").newInstance();
		connection = DriverManager.getConnection("jdbc:mysql://localhost/megatron", "root","");
		
		IEntityBuilder<StatusHistory> builder = new StatusHistoryEntityBuilder();
		IUnitOfWork uow = new UnitOfWork(connection);
		IRepository<StatusHistory> repo = new StatusHistoryRepository(connection,builder,uow);
		
		List<StatusHistory> statusHistories = repo.getAll();
		String html = "<html><body>";
		
		for(StatusHistory s : statusHistories)
		{	
			html+= s.getId() + " " + s.getReceiveDate() + " " + s.getInProgress() + " " + s.getReleaseDate() + " " + "<br>";
		}
		
		html+="</body></html>";
		
		
		PrintWriter out = response.getWriter();
		out.println(html);
		out.close();
		}
		catch (Exception e) 
		{
			
			e.printStackTrace();
		}
	}
}
