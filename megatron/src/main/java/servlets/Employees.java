package servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import repositories.IRepository;
import repositories.implementations.EmployeeRepository;
import unitOfWork.IUnitOfWork;
import unitOfWork.UnitOfWork;
import domain.Employee;
import entityBuilders.EmployeeEntityBuilder;
import entityBuilders.IEntityBuilder;

@WebServlet("/Employees")
public class Employees extends HttpServlet 
{
	private static final long serialVersionUID = 1L;
	private Connection connection;
       
    public Employees() 
    {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		try 
		{
			
		Class.forName("com.mysql.jdbc.Driver").newInstance();
		connection = DriverManager.getConnection("jdbc:mysql://localhost/megatron", "root","");
		
		IEntityBuilder<Employee> builder = new EmployeeEntityBuilder();
		IUnitOfWork uow = new UnitOfWork(connection);
		IRepository<Employee> repo = new EmployeeRepository(connection,builder,uow);
		
		List<Employee> employees = repo.getAll();
		String html = "<html><body>";
		
		for(Employee e : employees)
		{	
			html+= e.getId() + " " + e.getName() + " " + e.getSurname() + " " + e.getPhoneNumber() + " " + "<br>";
		}
		
		html+="</body></html>";
		
		
		PrintWriter out = response.getWriter();
		out.println(html);
		out.close();
		}
		catch (Exception e) 
		{
			
			e.printStackTrace();
		}
	}

}
