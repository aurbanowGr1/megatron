package servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import repositories.IRepository;
import repositories.implementations.ClientRepository;
import unitOfWork.IUnitOfWork;
import unitOfWork.UnitOfWork;
import domain.Client;
import entityBuilders.ClientEntityBuilder;
import entityBuilders.IEntityBuilder;

@WebServlet("/Clients")
public class Clients extends HttpServlet 
{
	private static final long serialVersionUID = 1L;
	private Connection connection;
       
    public Clients() 
    {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		try 
		{
			
		Class.forName("com.mysql.jdbc.Driver").newInstance();
		connection = DriverManager.getConnection("jdbc:mysql://localhost/megatron", "root","");
		
		IEntityBuilder<Client> builder = new ClientEntityBuilder();
		IUnitOfWork uow = new UnitOfWork(connection);
		IRepository<Client> repo = new ClientRepository(connection,builder,uow);
		
		List<Client> clients = repo.getAll();
		String html = "<html><body>";
		
		for(Client c : clients)
		{	
			html+= c.getId() + " " + c.getName() + " " + c.getSurname() + " " + c.getPhoneNumber() + " " + "<br>";
		}
		
		html+="</body></html>";
		
		
		PrintWriter out = response.getWriter();
		out.println(html);
		out.close();
		}
		catch (Exception e) 
		{
			
			e.printStackTrace();
		}
	}
}