package servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import repositories.IRepository;
import repositories.implementations.AddressRepository;
import unitOfWork.IUnitOfWork;
import unitOfWork.UnitOfWork;
import domain.Address;
import entityBuilders.AddressEntityBuilder;
import entityBuilders.IEntityBuilder;


@WebServlet("/Addresses")
public class Addresses extends HttpServlet 
{
	private static final long serialVersionUID = 1L;
	private Connection connection;
       
    public Addresses() 
    {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		try 
		{
			
		Class.forName("com.mysql.jdbc.Driver").newInstance();
		connection = DriverManager.getConnection("jdbc:mysql://localhost/megatron", "root","");
		
		IEntityBuilder<Address> builder = new AddressEntityBuilder();
		IUnitOfWork uow = new UnitOfWork(connection);
		IRepository<Address> repo = new AddressRepository(connection,builder,uow);
		
		List<Address> addresses = repo.getAll();
		String html = "<html><body>";
		
		for(Address a : addresses)
		{	
			html+= a.getId() + " " + a.getCountry() + " " + a.getCity() + " " + a.getStreet() + " " + a.getPostalCode() + " " + a.getHouseNumber() + " " + "<br>";
		}
		
		html+="</body></html>";
		
		
		PrintWriter out = response.getWriter();
		out.println(html);
		out.close();
		}
		catch (Exception e) 
		{
			
			e.printStackTrace();
		}
	}
}