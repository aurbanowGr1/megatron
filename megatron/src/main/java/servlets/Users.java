package servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import repositories.IRepository;
import repositories.implementations.UserRepository;
import unitOfWork.IUnitOfWork;
import unitOfWork.UnitOfWork;
import domain.User;
import entityBuilders.IEntityBuilder;
import entityBuilders.UserEntityBuilder;

@WebServlet("/Users")
public class Users extends HttpServlet 
{
	private static final long serialVersionUID = 1L;
	private Connection connection;
       
    public Users() 
    {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		try 
		{
			
		Class.forName("com.mysql.jdbc.Driver").newInstance();
		connection = DriverManager.getConnection("jdbc:mysql://localhost/megatron", "root","");
		
		IEntityBuilder<User> builder = new UserEntityBuilder();
		IUnitOfWork uow = new UnitOfWork(connection);
		IRepository<User> repo = new UserRepository(connection,builder,uow);
		
		List<User> users = repo.getAll();
		String html = "<html><body>";
		
		for(User u : users)
		{	
			html+= u.getId() + " " + u.getUsername() + " " + u.getEmail() + " " + u.getPassword() + " " + "<br>";
		}
		
		html+="</body></html>";
		
		
		PrintWriter out = response.getWriter();
		out.println(html);
		out.close();
		}
		catch (Exception e) 
		{
			
			e.printStackTrace();
		}
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		
	}

}
