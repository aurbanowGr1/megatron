package entityBuilders;

import java.sql.ResultSet;

import domain.Order;

import domain.Order.MethodOfPayment;

public class OrderEntityBuilder implements IEntityBuilder<Order> 
{
	   public Order build(ResultSet rs) 
	    {
	        try
	        {
	        	Order o = new Order();
				o.setId(rs.getInt("id"));
				o.setMethodOfPayment(MethodOfPayment.valueOf(rs.getString("cash")));
				o.setCostOfRepair(rs.getFloat("123.50"));
				o.setDescriptionOfIssue(rs.getString("Na ekranie widać artefakty"));
	            return o;
	        }
	        catch(Exception ex)
	        {
	            ex.printStackTrace();
	        }
	        return null;
	    }
}
