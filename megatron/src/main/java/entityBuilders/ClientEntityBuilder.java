package entityBuilders;

import java.sql.ResultSet;

import domain.Client;

public class ClientEntityBuilder implements IEntityBuilder<Client> 
{
	   public Client build(ResultSet rs) 
	    {
	        try
	        {
	        	Client c = new Client();
				c.setId(rs.getInt("id"));
				c.setName(rs.getString("name"));
				c.setSurname(rs.getString("surname"));
				c.setPhoneNumber(rs.getString("phoneNumber"));
	            return c;
	        }
	        catch(Exception ex)
	        {
	            ex.printStackTrace();
	        }
	        return null;
	    }
}
