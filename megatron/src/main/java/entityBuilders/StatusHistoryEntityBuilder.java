package entityBuilders;

import java.sql.ResultSet;

import domain.StatusHistory;


public class StatusHistoryEntityBuilder implements IEntityBuilder<StatusHistory> 
{
	   public StatusHistory build(ResultSet rs) 
	    {
	        try
	        {
	        	StatusHistory s = new StatusHistory();
				s.setId(rs.getInt("id"));
				s.setReceiveDate(rs.getDate("01-12-2013"));
				s.setInProgress(rs.getDate("02-12-2013"));
				s.setReleaseDate(rs.getDate("03-12-2013"));
	            return s;
	        }
	        catch(Exception ex)
	        {
	            ex.printStackTrace();
	        }
	        return null;
	    }
}
