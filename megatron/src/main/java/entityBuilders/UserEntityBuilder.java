package entityBuilders;

import java.sql.ResultSet;

import domain.User;

public class UserEntityBuilder implements IEntityBuilder<User> 
{
    public User build(ResultSet rs) 
    {
        try
        {
            User u = new User();
            u.setUsername(rs.getString("username"));
            u.setEmail(rs.getString("email"));
            u.setPassword(rs.getString("password"));
            return u;
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
        return null;
    }

}
