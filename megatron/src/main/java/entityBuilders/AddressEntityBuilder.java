package entityBuilders;

import java.sql.ResultSet;

import domain.Address;


public class AddressEntityBuilder implements IEntityBuilder<Address> 
{
	  public Address build(ResultSet rs) 
	    {
	        try
	        {
	            Address a = new Address();
	            a.setId(rs.getInt("id"));
				a.setCountry(rs.getString("Polska"));
				a.setCity(rs.getString("Gdansk"));
				a.setStreet(rs.getString("123456789"));
				a.setPostalCode(rs.getString("80-180"));
				a.setHouseNumber(rs.getString("12"));
	            return a;
	        }
	        catch(Exception ex)
	        {
	            ex.printStackTrace();
	        }
	        return null;
	    }
}
