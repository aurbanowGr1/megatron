package entityBuilders;

import java.sql.ResultSet;

import domain.Employee;


public class EmployeeEntityBuilder implements IEntityBuilder<Employee> 
{
	   public Employee build(ResultSet rs) 
	    {
	        try
	        {
	        	Employee e = new Employee();
				e.setId(rs.getInt("id"));
				e.setName(rs.getString("name"));
				e.setSurname(rs.getString("surname"));
				e.setPhoneNumber(rs.getString("phoneNumber"));
	            return e;
	        }
	        catch(Exception ex)
	        {
	            ex.printStackTrace();
	        }
	        return null;
	    }
}
