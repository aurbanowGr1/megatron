package repositories.implementations;

import java.sql.Connection;
import java.sql.SQLException;

import unitOfWork.IUnitOfWork;
import domain.User;
import entityBuilders.IEntityBuilder;

public class UserRepository extends RepositoryBase<User> 
{

    public UserRepository(Connection connection, IEntityBuilder<User> builder, IUnitOfWork uow) 
    {
        super(connection, builder, uow);
    }

    @Override
    protected void prepareUpdateQuery(User entity) throws SQLException 
    {
        update.setString(1, entity.getUsername());
        update.setString(2, entity.getEmail());
        update.setString(3, entity.getPassword());
        update.setLong(4, entity.getId());
    }

    @Override
    protected void prepareAddQuery(User entity) throws SQLException 
    {
        save.setString(1, entity.getUsername());
        save.setString(2, entity.getEmail());
        save.setString(3, entity.getPassword());
        
    }

    @Override
    protected String getTableName() 
    {
        return "user";
    }

    @Override
    protected String getCreateQuery() 
    {
        return "INSERT INTO user (username, email, password) VALUES (?, ?, ?)";
    }

    @Override
    protected String getUpdateQuery() 
    {
        return "UPDATE user SET username=?, email=?, password=? WHERE id=?";
    }

}
