package repositories.implementations;

import java.sql.Connection;
import java.sql.SQLException;

import unitOfWork.IUnitOfWork;
import domain.Client;
import entityBuilders.IEntityBuilder;

public class ClientRepository extends RepositoryBase<Client>
{

	public ClientRepository(Connection connection,IEntityBuilder<Client> builder, IUnitOfWork uow) 
	{
		super(connection, builder, uow);
		
	}

	@Override
	protected void prepareUpdateQuery(Client entity) throws SQLException
	{
		  	update.setString(1, entity.getName());
	        update.setString(2, entity.getSurname());
	        update.setString(3, entity.getPhoneNumber());
	        update.setLong(4, entity.getId());

	}

	@Override
	protected void prepareAddQuery(Client entity) throws SQLException 
	{
		 	save.setString(1, entity.getName());
	        save.setString(2, entity.getSurname());
	        save.setString(3, entity.getPhoneNumber());
	}

	@Override
	protected String getTableName() {
		
		return "client";
	}

	@Override
	protected String getUpdateQuery()
	{
		return //"UPDATE client SET (name, surname, phoneNumber)=(?, ?, ?) WHERE id=?";
		 "UPDATE client SET name=?, surname=?, phoneNumber=? WHERE id=?";
	}

	@Override
	protected String getCreateQuery() 
	{
		
		return "INSERT INTO client (name, surname, phoneNumber) VALUES (?, ?, ?)";
	}

}
