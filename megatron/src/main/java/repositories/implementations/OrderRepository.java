package repositories.implementations;

import java.sql.Connection;
import java.sql.SQLException;



import unitOfWork.IUnitOfWork;
import domain.Order;
import entityBuilders.IEntityBuilder;

public class OrderRepository extends RepositoryBase<Order>
{

	public OrderRepository(Connection connection, IEntityBuilder<Order> builder, IUnitOfWork uow)
	{
		super(connection, builder, uow);
		
	}
	
	@Override
	protected void prepareUpdateQuery(Order entity) throws SQLException
	{
		  	update.setString(1, (entity.getMethodOfPayment()).toString());
	        update.setFloat(2, entity.getCostOfRepair());
	        update.setString(3, entity.getDescriptionOfIssue());
	        update.setLong(4, entity.getId());

	}

	@Override
	protected void prepareAddQuery(Order entity) throws SQLException 
	{
			save.setString(1, (entity.getMethodOfPayment()).toString());
		 	save.setFloat(2, entity.getCostOfRepair());
	        save.setString(3, entity.getDescriptionOfIssue());
	}

	@Override
	protected String getTableName() {
		
		return "order";
	}

	@Override
	protected String getUpdateQuery()
	{
		return "UPDATE order SET methodOfPayment=?, costOfRepair=?, descriptionOfIssue=? WHERE id=?";
	}

	@Override
	protected String getCreateQuery() 
	{
		
		return "INSERT INTO order(methodOfPayment, costOfRepair, descriptionOfIssue) VALUES (?, ?, ?)";
	}

}
