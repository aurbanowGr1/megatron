package repositories.implementations;

import java.sql.Connection;
import java.sql.SQLException;

import unitOfWork.IUnitOfWork;
import domain.StatusHistory;
import entityBuilders.IEntityBuilder;

public class StatusHistoryRepository extends RepositoryBase<StatusHistory> 
{

	public StatusHistoryRepository(Connection connection, IEntityBuilder<StatusHistory> builder, IUnitOfWork uow) 
	{
		super(connection, builder, uow);
		
	}
	
	@Override
	protected void prepareUpdateQuery(StatusHistory entity) throws SQLException
	{
		  	
			update.setDate(1, entity.getReceiveDate());
			update.setDate(2, entity.getInProgress());
			update.setDate(3, entity.getReleaseDate());
			update.setLong(4, entity.getId());
	}

	@Override
	protected void prepareAddQuery(StatusHistory entity) throws SQLException 
	{
		save.setDate(1, entity.getReceiveDate());
		save.setDate(2, entity.getInProgress());
		save.setDate(3, entity.getReleaseDate());
	}

	@Override
	protected String getTableName() {
		
		return "statusHistory";
	}

	@Override
	protected String getUpdateQuery()
	{
		return "UPDATE statusHistory SET receiveDate=?, inProgress=?, releaseDate=? WHERE id=?";
	}

	@Override
	protected String getCreateQuery() 
	{
		
		return "INSERT INTO statusHistory(receiveDate, inProgress, releaseDate) VALUES (?, ?, ?)";
	}

}
