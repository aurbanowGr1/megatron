package repositories.implementations;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import repositories.IRepository;
import unitOfWork.IUnitOfWork;
import unitOfWork.IUnitOfWorkRepository;
import domain.Entity;
import entityBuilders.IEntityBuilder;

public abstract class RepositoryBase<TEntity extends Entity> implements IRepository<TEntity>, IUnitOfWorkRepository
{

	protected PreparedStatement selectAll;
	protected PreparedStatement selectById;
	protected PreparedStatement update;
	protected PreparedStatement delete;
	protected PreparedStatement save;
	protected Connection connection;
	protected IEntityBuilder<TEntity> builder;
	protected IUnitOfWork uow;

	protected RepositoryBase(Connection connection, IEntityBuilder<TEntity> builder, IUnitOfWork uow) 
	{

		this.builder = builder;
		try 
		{
			this.uow = uow;
			this.connection = connection;
			update = connection.prepareStatement(getUpdateQuery());
			delete = connection.prepareStatement("DELETE FROM " + getTableName() + " WHERE id=?");
			save = connection.prepareStatement(getCreateQuery());
			selectAll = connection.prepareStatement("SELECT * FROM " + getTableName());
			selectById = connection.prepareStatement("SELECT * FROM " + getTableName() + " WHERE id=?");
		} 
		catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	// @Override
	public TEntity get(int id) 
	{
		try 
		{
			selectById.setInt(1, id);
			ResultSet rs = selectById.executeQuery();
			while (rs.next()) 
			{
				return builder.build(rs);
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}

		return null;
	}

	// @Override
	public List<TEntity> getAll() 
	{

		List<TEntity> result = new ArrayList<TEntity>();
		try 
		{
			ResultSet rs = selectAll.executeQuery();
			while (rs.next()) 
			{
				result.add(builder.build(rs));
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}

		return result;
	}

	// @Override
	public void persistAdd(Entity entity) 
	{
		try 
		{
			prepareAddQuery((TEntity) entity);
			save.executeUpdate();
		} 
		catch (Exception ex) 
		{
			ex.printStackTrace();
		}
	}
	
	
	public void delete(TEntity entity)
	{
		uow.markAsDeleted(entity, this);
	}
	 
	
	public void add(TEntity entity)
	{
		uow.markAsNew(entity, this);
	}
	 
	
	public void update(TEntity entity)
	{
		uow.markAsChanged(entity, this);
	}

	// @Override
	public void persistDelete(Entity entity) {
		
		try
		{
			delete.setLong(1, entity.getId());
			delete.executeUpdate();
		} 
		catch (Exception ex) 
		{
			ex.printStackTrace();
		}

	}

	// @Override
	public void persistUpdate(Entity entity)
	{
		try
		{
			prepareUpdateQuery((TEntity) entity);
			update.executeUpdate();
		} 
		catch (Exception ex)
		{
			ex.printStackTrace();
		}
	}

	protected abstract void prepareUpdateQuery(TEntity entity) throws SQLException;

	protected abstract void prepareAddQuery(TEntity entity) throws SQLException;

	protected abstract String getTableName();

	protected abstract String getUpdateQuery();

	protected abstract String getCreateQuery();
}
