package repositories.implementations;

import java.sql.Connection;
import java.sql.SQLException;

import unitOfWork.IUnitOfWork;
import domain.Employee;
import entityBuilders.IEntityBuilder;

public class EmployeeRepository extends RepositoryBase<Employee>
{
	public EmployeeRepository(Connection connection, IEntityBuilder<Employee> builder, IUnitOfWork uow) 
	{
		super(connection, builder, uow);
	}

	@Override
	protected void prepareUpdateQuery(Employee entity) throws SQLException
	{
		  	update.setString(1, entity.getName());
	        update.setString(2, entity.getSurname());
	        update.setString(3, entity.getPhoneNumber());
	        update.setLong(4, entity.getId());

	}

	@Override
	protected void prepareAddQuery(Employee entity) throws SQLException 
	{
		 	save.setString(1, entity.getName());
	        save.setString(2, entity.getSurname());
	        save.setString(3, entity.getPhoneNumber());
	}

	@Override
	protected String getTableName() {
		
		return "employee";
	}

	@Override
	protected String getUpdateQuery()
	{
		return "UPDATE employee SET name=?, surname=?, phoneNumber=? WHERE id=?";
	}

	@Override
	protected String getCreateQuery() 
	{
		
		return "INSERT INTO employee (name, surname, phoneNumber) VALUES (?, ?, ?)";
	}
}
