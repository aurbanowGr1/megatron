package repositories.implementations;

import java.sql.Connection;
import java.sql.SQLException;

import unitOfWork.IUnitOfWork;
import domain.Address;
import entityBuilders.IEntityBuilder;

public class AddressRepository extends RepositoryBase<Address> 
{

	public AddressRepository(Connection connection, IEntityBuilder<Address> builder, IUnitOfWork uow)
	{
		super(connection, builder, uow);
		
	}
	
	@Override
	protected void prepareUpdateQuery(Address entity) throws SQLException
	{
		  	
			update.setString(1, entity.getCountry());
			update.setString(2, entity.getCity());
			update.setString(3, entity.getStreet());
			update.setString(4, entity.getPostalCode());
			update.setString(5, entity.getHouseNumber());
			update.setLong(6, entity.getId());
	}

	@Override
	protected void prepareAddQuery(Address entity) throws SQLException 
	{
		save.setString(1, entity.getCountry());
		save.setString(2, entity.getCity());
		save.setString(3, entity.getStreet());
		save.setString(4, entity.getPostalCode());
		save.setString(5, entity.getHouseNumber());
	}

	@Override
	protected String getTableName() {
		
		return "address";
	}

	@Override
	protected String getUpdateQuery()
	{
		return "UPDATE address SET country=?, city=?, street=?, postalCode=?, houseNumber=? WHERE id=?";
	}

	@Override
	protected String getCreateQuery() 
	{
		
		return "INSERT INTO address(country, city, street, postalCode, houseNumber) VALUES (?, ?, ?, ?, ?)";
	}

}
