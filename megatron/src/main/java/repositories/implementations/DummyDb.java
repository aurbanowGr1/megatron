package repositories.implementations;

import java.util.*;

import domain.*;


public class DummyDb
{

	
	public List<Client> clients;
	public List<User> users;
	public List<Address> addresses;
	public List<Order> orders;
	public List<Employee> employees;
	public List<StatusHistory> statusHistory;
	
	public DummyDb()
	{
		clients = new ArrayList<Client>();
		users = new ArrayList<User>();
		addresses = new ArrayList<Address>();
		orders =  new ArrayList<Order>();
		employees = new ArrayList<Employee>();
		statusHistory = new ArrayList<StatusHistory>();
	}
	
	
}
