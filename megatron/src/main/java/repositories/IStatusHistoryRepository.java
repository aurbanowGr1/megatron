package repositories;

import java.sql.Date;
import java.util.List;

import domain.Order;
import domain.StatusHistory;

public interface IStatusHistoryRepository extends IRepository<StatusHistory>
{
	public StatusHistory getReceiveDate(Date receiveDate);
	public StatusHistory getInProgress(Date inProgress);
	public StatusHistory getReleaseDate(Date releaseDate);
	public List<StatusHistory> getStatusHistoryByOrders(Order order);
}
