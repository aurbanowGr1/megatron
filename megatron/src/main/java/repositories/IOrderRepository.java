package repositories;

import java.util.List;

import domain.*;
import domain.Order.MethodOfPayment;

public interface IOrderRepository extends IRepository<Order>
{
	
	public Order getDescriptionOfIssue(String descriptionOfIssue);
	public Order getMethodOfPayment(MethodOfPayment methodOfPayment);
	public Order getCostOfRepair(float costOfRepair);
	public Order getOrderByStatusHistory(StatusHistory statusHistory);
	public List<Order> getOrdersByClient(Client client);
	public List<Order> getOrdersByEmployee(Employee employee);
}
