package repositories;

import java.util.List;

import domain.Order;
import domain.Employee;

public interface IEmployeeRepository extends IRepository<Employee>
{
	public Employee getEmployeeByName(String name);
	public Employee getEmployeeBySurname(String surname);
	public Employee getEmployeeByPhoneNumber(String phoneNumber);
	public List<Employee> getEmployeesByOrder(Order order);

}
