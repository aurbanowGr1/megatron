package repositories;

import java.util.List;

import domain.Address;
import domain.Client;
import domain.Employee;

public interface IAddressRepository extends IRepository<Address>
{
	public List<Address> getAddressesByClient(Client client);
	public Address getAddressByEmployee(Employee employee);
	public Address getAddressByCountry(String country);
	public Address getAddressByCity(String city);
	public Address getAddressByStreet(String street);
	public Address getAddressByPostalCode(String postalCode);
	public Address getAddressByHouseNumber(String houseNumber);

}
