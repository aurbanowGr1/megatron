package repositories;

import domain.*;

public interface IUserRepository extends IRepository<User> 
{
	
	public User getUserByUsername(String username);
	public User getUserByEmail(String email);
	public User getUserByClient(Client client);
	
}
