package repositories;

import domain.Address;
import domain.Client;
import domain.Order;

public interface IClientRepository extends IRepository<Client>
{
	
	public Client getClientByName(String name);
	public Client getClientBySurname(String surname);
	public Client getClientByPhoneNumber(String phoneNumber);
	public Client getClientByAddress(Address address);
	public Client getClientByOrder(Order order);
	
}
