package service;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import domain.Order;
import domain.Order.MethodOfPayment;

public class OrderManager 
{
	
	private Connection connection;
	
	private String url = "jdbc:hsqldb:hsql://localhost/workdb";
	
	private String createTableOrder = "CREATE TABLE Order(id bigint GENERATED BY DEFAULT, methodOfPayment varchar(15), costOfRepair float, descriptionOfIssue varchar(200))";
	
	private PreparedStatement addOrderStmt;
	private PreparedStatement deleteOrderStmt;
	private PreparedStatement getAllOrderStmt;
	
	private Statement statement;
	
	
	
	public OrderManager()
	{
		
		try
		{
			connection = DriverManager.getConnection(url);
			statement = connection.createStatement();
			
			ResultSet rs = connection.getMetaData().getTables(null, null, null, null);
			boolean tableExists = false;
			
			while(rs.next())
			{
				if("Order".equalsIgnoreCase(rs.getString("TABLE_NAME")));
				{
					tableExists = true;
					break;
				}
			}
			
			if(!tableExists)
			{
				statement.executeUpdate(createTableOrder);
			}
			
			addOrderStmt = connection.prepareStatement("INSERT INTO User (id, costOfRepair, descriptionOfIssue) VALUES (?, ?, ?)");
			deleteOrderStmt = connection.prepareStatement("DELETE FROM Order");
			getAllOrderStmt = connection.prepareStatement("SELECT id, costOfRepair, descriptionOfIssue");
					
		}
		
		catch(SQLException e)
		{
		e.printStackTrace();	
		}
		
			
	}
	
	
	Connection getConnection()
	{
		return connection;
	}
	
	
	void clearOrder()
	{
		try
		{
			deleteOrderStmt.executeUpdate();
		}
		
		catch(SQLException e)
		{
			e.printStackTrace();
		}
	}
	
	
	public int addOrder(Order order)
	{
		
		int count = 0;
		
		try
		{
			addOrderStmt.setInt(1, order.getId());
			addOrderStmt.setString(2, (order.getMethodOfPayment()).toString());
			addOrderStmt.setFloat(3, order.getCostOfRepair());
			addOrderStmt.setString(4, order.getDescriptionOfIssue());
			
			count = addOrderStmt.executeUpdate();
		}
		
		catch(SQLException e)
		{
			e.printStackTrace();
		}
		
		return count;
	}
	
	
	public List<Order> getAllOrders()
	{
		List<Order> orders = new ArrayList<Order>();
		
		try
		{
			ResultSet rs = getAllOrderStmt.executeQuery();
			
			while(rs.next())
			{
				Order o = new Order();
				o.setId(rs.getInt("id"));
				o.setMethodOfPayment(MethodOfPayment.valueOf(rs.getString("cash")));
				o.setCostOfRepair(rs.getFloat("123.50"));
				o.setDescriptionOfIssue(rs.getString("Na ekranie widać artefakty"));
				
				orders.add(o);
				
			}
		}
		
		catch(SQLException e)
		{
			e.printStackTrace();
		}
		
		return orders;
		
	}
}
