package megatron.megatron;

import java.sql.Connection;
import java.sql.DriverManager;

import repositories.IRepository;
import repositories.implementations.UserRepository;
import unitOfWork.IUnitOfWork;
import unitOfWork.UnitOfWork;
import domain.User;
import entityBuilders.IEntityBuilder;
import entityBuilders.UserEntityBuilder;

public class App 
{
    public static void main( String[] args )
    {
    	try
        {
    		Class.forName("com.mysql.jdbc.Driver").newInstance();
            Connection connection = DriverManager.getConnection("jdbc:mysql://localhost/megatron", "root","");
            
            IEntityBuilder<User> builder = new UserEntityBuilder();
            IUnitOfWork uow = new UnitOfWork(connection);
            IRepository<User> repo = new UserRepository(connection, builder, uow);
            User u = new User();
            
            u.setId(1);
            u.setUsername("Korzen"); 
            u.setEmail("korzen@wp.pl");
            u.setPassword("qwerty");
           
            
            repo.add(u);
            uow.commit();
            connection.close();
        } 
    	catch (Exception e) 
        {
            e.printStackTrace();
        }
    	System.out.println("Koniec");
    }
}
